 This is a slightly modified version, (a "fork", as they call it), of the Scratch Space server version 1.5, more commonly known as Chat.PY 2.0, originally written by Mangie on scratch.mit.edu and modified by zippynk on scratch.mit.edu. The modifications include bug fixes, code to make administering the server more user friendly, and the removal of obsolete features.

This software, including both the code originally provided by Magnie, and the additions made by zippynk, is provided to you under the Creative Commons BY-NC-SA 3.0 License.

The original version of the software, created entirely by Magnie, is downloadable at http://sourceforge.net/projects/scratchspace/

To run the server, cd into the directory of the project and run "python server.py" in a terminal. If it is your first time running, you will be prompted to set up a default room and account. To allow standard clients to access, use 42001 for the port.
